---
layout: post
title: "Danzafuera"
date: 2013-11-02
description: FESTIVAL INTERNACIONAL DE DANZA CONTEMPORÁNEA, PERFORMANCE Y ACCIONES TRANSDISCIPLINARIAS
image: /assets/images/danzafuera2.png
author: Jor Mongan
tags:
  - Obra
---

FESTIVAL INTERNACIONAL DE DANZA CONTEMPORÁNEA, PERFORMANCE Y ACCIONES TRANSDISCIPLINARIAS

[https://danzafuera.wixsite.com/festival](https://danzafuera.wixsite.com/festival)

<p>Danzafuera busca contener diversas prácticas y producciones artísticas que se desarrollan en contextos determinados, atravesadas por procesos de investigación que toman al cuerpo y al lenguaje del movimiento como principal soporte y medio.
Nuestra programación está conformada por obras de danza, intervenciones  y acciones performáticas transdisciplinarias tanto en espacios abiertos, como en espacios cerrados de la ciudad de La Plata.
Desde la primera edición en 2013, el festival ensaya preguntas en torno a los límites de la danza, ampliando sus fronteras tanto en un sentido espacial como conceptual, llevándola fuera de sus espacios de circulación habituales y por qué no, también fuera de sí misma. A partir de este objetivo, una parte de su programación incluye intancias de formación intensivas (las llamamos residencias) con el objetivo de incentivar la investigación y el estudio de los territorios donde se trabaja.
Danzafuera apuesta a una programación abierta y gratuita porque considera que de esta manera posibilita un mayor acceso. Cada edición del festival reúne a unxs 150 artistas tanto locales como de otros lugares del mundo. Es por esto que creemos que el festival se volvió una fuente de trabajo para muchxs artistas y gestorxs y se asumió el compromiso de que todas las actividades realizadas para el festival sean remuneradas.
Consideramos  al festival una obra que se (re)crea cada año e intenta (re)situarse y actualizarse cada edición.</p>

<br>
<br>
<br>

<p><strong>Dirección general y curaduría:</strong> Constanza Copello, Jorgelina Mongan, Mariana Saéz y Julieta Scanferla</p>
<p><strong>Producción:</strong> Delfina Serra</p>
<p><strong>Comunicación & difusión:</strong> Daniela Camezzana</p>
<p><strong>Prensa:</strong> Daniel Franco</p>
<p><strong>Dirección técnica:</strong> René Mantiñán</p>
<p><strong>Diseño de imagen:</strong> Dani Lorenzo</p>
<p><strong>Registro fotográfico y audiovisual:</strong> Matías Adhemar</p>

<hr>
![foto](/assets/images/danzafuera3.png)
<hr>

![foto](/assets/images/danzafuera4.jpg)
<hr>

![foto](/assets/images/danzafuera5.jpg)
