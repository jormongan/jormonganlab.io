---
layout: post
title: "Conresponder"
date: 2018-11-23
description: Conresponder
image: /assets/images/conresponder.png
author: Jor Mongan
tags:
  - Obra
---

Pieza de danza en la que participó la artista visual Marina de Caro como bailarina y la artista sonora Alma Laprida,
en donde se abordaron las preposiciones: _sin_, _con_, _desde_, _contra_ y _entre_. Fue presentada en el Centro Universitario de Arte de la
UNLP, en la ciudad de La Plata en noviembre del 2018.

![foto](/assets/images/conresponder1.png)
<hr>

![foto](/assets/images/conresponder2.png)
