---
layout: post
title: "Siete intentos para acompañar la muerte de un volcán"
date: 2021-11-06
description: Siete intentos para acompañar la muerte de un volcán
image: /assets/images/siete.jpg
author: Jor Mongan
tags:
  - Obra
---

<p>Guillermina y Jorgelina Mongan</p>

<p>Música de Cecilia Castro</p>

<p>Presentado en  el marco de "El tiempo de las cosas",  coordinado por Barbara Hang y Agustina Muñoz. CCK . Noviembre 2021</p>

<p>Entre la última erupción de un volcán y el tiempo que transcurre hacia la próxima, vida y muerte se encuentran en lo latente.
La niebla ígnea revela la imagen del fuego intuyendo el tiempo. ¿Cuándo muere un volcán? ¿Cuándo mueren nuestros muertos?
Siete intentos para acompañar la muerte de un volcán reúne un conjunto de gestos e invocaciones donde nuestros muertos devienen geógrafos.
Como dirá Vinciane Despret, “Hay que situar al muerto, hacerle un lugar; el aquí se vació y hay que construir el allá”.</p>

<hr>
<p><strong>Primer intento:</strong> niebla ígnea. En la desmaterialización hay movimiento.</p>
<p><strong>Segundo intento:</strong> placas tectónicas. El accidente irrumpe en el futuro.</p>
<p><strong>Tercer intento:</strong> magma. La experiencia de la presencia.</p>
<p><strong>Cuarto intento:</strong> tierra. Los muertos son geógrafos.</p>
<p><strong>Quinto intento:</strong> lava. Los fantasmas de todos tienen algo en común.</p>
<p><strong>Sexto intento:</strong> cráter. Connivencias de estratos ascendentes.</p>
<p><strong>Séptimo intento:</strong> ceniza. Latencia y continuidad.</p>

![foto](/assets/images/siete1.jpg)
<hr>
![foto](/assets/images/siete2.jpg)
<hr>
![foto](/assets/images/siete3.jpg)
