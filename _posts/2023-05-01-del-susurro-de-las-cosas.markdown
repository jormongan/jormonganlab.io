---
layout: post
title: "Del Susurro de las cosas"
date: 2023-05-01
description: Partiendo de un recorrido perceptivo y sensible de la muestra Uso y Función objetualidades poético políticas de la Esma, amplificaremos las sensaciones que nuestros cuerpos contienen sobre el Terrorismo de Estado de la Argentina para desarrollar una performance que dialogue con los gestos de algunas de las obras expuestas, como así también con la arquitectura del predio y su memoria.
image: /assets/images/susurro1.jpg
author: Jor Mongan
tags:
  - Obra
---

Partiendo de un recorrido perceptivo y sensible de la muestra Uso y Función objetualidades poético políticas de la Esma, amplificaremos las sensaciones que nuestros cuerpos contienen sobre el Terrorismo de Estado de la Argentina para desarrollar una performance que dialogue con los gestos de algunas de las obras expuestas, como así también con la arquitectura del predio y su memoria.

Será la vibración del sonido de los cuerpos la que dialogue con el pasado y lo resignifique en nuestro presente.


Performers: Manuela Piqué y Jorgelina Mongan

<hr>
![foto](/assets/images/susurro2.jpg)
<hr>

![foto](/assets/images/susurro3.jpg)
<hr>

![foto](/assets/images/susurro4.jpg)
<hr>
