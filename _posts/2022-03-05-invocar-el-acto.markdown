---
layout: post
title: "Invocar el acto"
date: 2022-03-05
description: Invocar el acto Bienal de Performance 2021
image: /assets/images/invocar-el-acto.jpg
author: Jor Mongan
tags:
  - Obra
---

Bienal de Performance 2021. CABA en Chela.

La performance de Jorgelina Mongan, Gonzalo Lagos, Guillermina Mongan y Luciana Lamothe junto a la música de Miguel Garutti e Ignacio Llobera surge de un largo proceso guiado por las siguientes preguntas
¿Cómo extrañar y provocar lo humano? ¿Qué tipo de gestos aparecen entre la reverberancia de la materia, el espacio y la invocación?
¿Cómo un espacio, una objetualidad nos induce hacia un estado de provocación?¿Qué invocamos con otrxs al percibirnos ante un mismo acto?
¿Cómo se resignifican los gestos al convocar presencias inmateriales?¿Cuánto conlleva de provocador invocar con otrxs?
¿Es posible que lo provocador de un gesto pueda encontrarse más allá de la relación entre su forma y su intención?

<p>Concepto: Luciana Lamothe, Guillermina Mongan, Gonzalo Lagos y Jorgelina Mongan</p>
<p>Performers: Daniela Camezzana, Julia Hadida, Victoria Hernandez, Mariana Moreno, Nicolás Salvatierra</p>
<p>Esculturas y diseño espacial : Luciana Lamothe</p>
<p>Música: Miguel Garutti e Ignacio Llobera</p>
<p>Asistencia en espacio e iluminación: Alfio Demestre</p>
<p>Dirección: Guillermina Mongan, Gonzalo Lagos y Jorgelina Mongan</p>
<p>Agradecimientos: Victoria, Grupo ENAS, ACIADIP, Juan Pablo Rosset</p>

![foto](/assets/images/invocar-el-acto-2.jpg)
<hr>

![foto](/assets/images/invocar-el-acto-3.jpg)
<hr>

Video [https://vimeo.com/695725827/b77b9d523a](https://vimeo.com/695725827/b77b9d523a)
