---
layout: post
title: "Cartografíás de la Memoria II"
date: 2022-03-20
description: Cartografíás de la Memoria II. Intervención performática
image: /assets/images/cartografias1.png
author: Jor Mongan
tags:
  - Obra
---


<p>Fuimos convocadas junto a Guillermina Mongan por el área de danza del Centro Cultural Conti para realizar una intervención en el marco de Cartografías de la memoria en su segundo año, esta caminata sonora propone un relato histórico, literario y performático que evoca parte de los sucedido durante la última dictadura militar. (Marzo 2022, CABA)</p>
<p>Performer: Manuela Piqué.</p>

![foto](/assets/images/cartografias2.png)
<hr>

![foto](/assets/images/cartografias3.png)
<hr>

![foto](/assets/images/cartografias4.png)
<hr>
