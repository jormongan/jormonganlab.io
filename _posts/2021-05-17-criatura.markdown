---
layout: post
title: "Criatura nómada"
date: 2021-05-16
description: Criatura nómada
image: /assets/images/criatura.jpg
author: Jor Mongan
tags:
  - Obra
---

<p>¿Qué tipo de gestos, sonidos y desplazamientos se producen en una criatura que no es ni humana, ni animal, ni vegetal? ¿Cuáles son sus movimientos? ¿Cómo respira? ¿Qué atmósfera es capaz de construir?</p>

<p>Criatura nómada es una investigación con y desde la corporalidad acerca de lo previo a la construcción de lo que nos constituye y forma/deforma como humanxs.</p>

<p>Esta performance deviene de una serie de ejercicios basados en la respiración y en un trabajo de investigación de años anteriores sobre la construcción de un posible cuerpo prelingüístico.</p>

<p>Estuvo en el marco de la muestra colectiva Fuera de Cuadro, Munar, Buenos Aires.</p>

<iframe title="vimeo-player" src="https://player.vimeo.com/video/604921918?h=81e757bb46" width="640" height="360" frameborder="0" allowfullscreen></iframe>

![foto](/assets/images/criatura2.jpg)
<hr>
![foto](/assets/images/criatura4.jpg)
<hr>
![foto](/assets/images/criatura5.jpg)
