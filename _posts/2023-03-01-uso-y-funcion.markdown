---
layout: post
title: Uso y Función - Objetualidades poético-políticas de la ESMA
date: 2023-03-01
description: Registro audiovisual y objetos de la performance para Cartografías de la Memoria II en el marco de la exposición "Uso y función"
image: /assets/images/uso1.jpg
author: Jor Mongan
tags:
  - Obra
---

¿Qué pueden aportar un conjunto de producciones simbólicas realizadas con elementos de la ESMA? Esta muestra nos dispone a transitar colectivamente una serie de preguntas en torno a la dimensión indicial/ sígnica/ simbólica de los objetos y el aporte que pueden producirse desde producciones poético políticas a los trabajos de memoria.

Uso y función es una exhibición que propone- a través del acceso a documentos, archivos y objetos patrimoniales- un entrecruzamiento entre artistas y equipos de trabajo de diferentes instituciones de la Ex ESMA. El resultado de este encuentro es la serie de producciones poético-políticas que se exhiben en las salas del Conti.

Las operatorias artísticas profundizan sobre conceptos que van desde el disciplinamiento del cuerpo, la arquitectura metabólica, el léxico militar, hasta el lenguaje codificado del Grupo de Tareas de la ESMA.

Esta muestra pone en juego lo patrimonial como una reminiscencia, un fragmento, una esquirla, que aporta nuevos sentidos.


De Jorgelina y Guillermina Mongan

Performer: Manuela Piqué

Centro Cultural de la memoria Haroldo Conti. Ciudad de Buenos Aires. Argentina.

<hr>
![foto](/assets/images/uso2.jpg)
<hr>

![foto](/assets/images/uso3.jpg)
<hr>
