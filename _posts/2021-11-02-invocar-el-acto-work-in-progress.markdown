---
layout: post
title: "Invocar el acto (work in progress)"
date: 2021-11-02
description: Invocar el acto. Presentación work in progress
image: /assets/images/invocar.jpg
author: Jor Mongan
tags:
  - Obra
---

<p>Jorgelina Mongan, Guillermina Mongan y Gonzalo Lagos.</p>

<p>Estos ejercicios formaron parte del Ciclo sin Forma 2021. El Gran Vidrio. Córdoba.</p>

<p>¿Cómo extrañar y provocar lo humano? ¿Qué tipo de gestos aparecen entre la reverberancia de la materia, el espacio y la invocación? ¿Cómo un espacio, una objetualidad nos induce hacia un estado de provocación? ¿Qué invocamos con otres al percibirnos ante un mismo acto? ¿Cómo se resignifican los gestos al convocar presencias inmateriales? ¿Cuánto conlleva de provocador invocar con otres? ¿Es posible que lo provocador de un gesto pueda encontrarse más allá de la relación entre su forma y su intención?</p>

<p>Proyecto a estrenarse el 4 de marzo 2022 en el marco de la Bienal de Performance, Buenos Aires.</p>

![foto](/assets/images/invocar02.jpg)
<hr>
![foto](/assets/images/invocar03.jpg)
